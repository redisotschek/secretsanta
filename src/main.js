import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import VueClipboard from 'vue-clipboard2'
import VueSweetAlert2 from 'vue-sweetalert2'
import {firestorePlugin} from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.use(VueClipboard)
Vue.use(VueSweetAlert2)
Vue.use(firestorePlugin)

Vue.config.productionTip = false

firebase.initializeApp({
  projectId: 'secret-santa-ae108', 
  databaseURL: 'https://secret-santa-ae108.firebaseio.com/'
 })

 export const db = firebase.firestore()

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
