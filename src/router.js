import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Recipient from './views/Recipient.vue'
import Create from './views/Create.vue'
import Admin from './views/Admin.vue'
import Register from './views/Register.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/create',
      name: 'create',
      component: Create
    },
    {
      path: '/admin',
      name: 'admin',
      component: Admin
    },
    {
      path: '/register',
      name: 'register',
      component: Register
    },
    {
      path: '/recipient',
      name: 'recipient',
      component: Recipient
    }
  ]
})
